<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>
		Sondages
	</title>
	<meta name="apple-mobile-web-app-capable" content="yes" /> 
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<link rel="apple-touch-icon" href="<?php echo RACINE?>img/apple-icon-sondage.png" /> 
	<link href="<?php echo RACINE?>css/reset.css" rel="stylesheet" type="text/css" />
	<!--<link href="<?php echo RACINE?>css/jqueryUi.css" rel="stylesheet" type="text/css" />-->
	<link href="<?php echo RACINE?>css/style.css" rel="stylesheet" type="text/css" />

	<?php echo $addJs?>

</head>
<body>
	<?php echo $content_for_layout; ?>
</body>
</html>

