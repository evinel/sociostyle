<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="<?php if($metaDescription) echo $metaDescription; else echo "Une réalisation Ethics group"?>" />
	<title>SOCIO STYLE</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="apple-touch-icon" href="<?php echo RACINE?>img/icon_framework.png" />
	<link href="<?php echo RACINE?>css/reset.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo RACINE?>css/font/stylesheet.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo RACINE?>css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo RACINE?>css/jqueryUi.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo RACINE?>css/publicStyle.css" rel="stylesheet" type="text/css" />

	<?php echo $addCss?>

	<script src="<?php echo RACINE?>js/jquery-3.1.1.min.js"></script>
	<script src="<?php echo RACINE?>js/jqueryUi.js"></script>
	<?php echo $addJs?>

</head>
<body>	
	<div class="container">
		<?php echo $content_for_layout; ?>	
	</div>
</body>
</html>