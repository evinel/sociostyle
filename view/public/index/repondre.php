<div class="textcenter"><br><img src="<?php echo RACINE?>img/public/airbus_logo.png"></div><br><br>

<form method="POST" id="myForm"  action="">
	<fieldset>
		<legend>
			<?php if($_SESSION['profil_additionnel'])
			echo '<div class="textcenter titre_profil_additionnel">Estimation du profil de  '.$profil_additionnel->nomprenom.'</div>';
			else
				echo '<div class="textcenter titre_profil_additionnel">'.$_SESSION['nomprenom'].', complétez votre profil</div>';
			?>
		</legend>

		<div class="clearfix"></div>

		<?php 
		foreach($intitules as $key=>$intitule)
		{
			?>
			<div class="question">
				<div class="libelle">
					<div class="cas1"><?php echo $intitule['gauche_fr'] ?></div>
					<div class="cas2"><?php echo $intitule['droite_fr'] ?></div>
				</div>
				<div class="slider" id="slider-range-max-<?php echo $intitule['id'] ?>">
					
				</div>
				<input type="text" name="amount[<?php echo $intitule['id'] ?>]" id="amount-<?php echo $intitule['id'] ?>" readonly class="inputformulaire" value="<?php /*echo rand(0,3);*/ echo 'NA'?>">
			</div>
			<?php
		}
		?>

        <?php
        foreach($empathies as $key=>$empathie)
        {
            ?>
            <div class="question">
                <div class="libelle">
                    <div class="cas1"><?php echo $empathie['gauche_fr'] ?></div>
                    <div class="cas2"><?php echo $empathie['droite_fr'] ?></div>
                </div>
                <div class="slider" id="slider-range-max-empathie-<?php echo $empathie['id'] ?>">

                </div>
                <input type="hidden" id="amount-empathie-calcule-[<?php echo $empathie['id'] ?>]" name="amount-empathie-calcule[<?php echo $empathie['id'] ?>]" value="<?php /*echo rand(0,3);*/ echo 'NA'?>">
                <input type="text" name="amount-empathie[<?php echo $empathie['id'] ?>]" id="amount-empathie-<?php echo $empathie['id'] ?>" readonly class="inputformulaire" value="<?php /*echo rand(0,3);*/ echo 'NA'?>">
            </div>
            <?php
        }
        ?>
        <?php
        foreach($flexibilites as $key=>$flexibilite)
        {
            ?>
            <div class="question">
                <div class="libelle">
                    <div class="cas1"><?php echo $flexibilite['gauche_fr'] ?></div>
                    <div class="cas2"><?php echo $flexibilite['droite_fr'] ?></div>
                </div>
                <div class="slider" id="slider-range-max-flexibilite-<?php echo $flexibilite['id'] ?>">

                </div>
                <input type="hidden" id="amount-flexibilite-calcule-[<?php echo $flexibilite['id'] ?>]" name="amount-flexibilite-calcule[<?php echo $flexibilite['id'] ?>]" value="<?php /*echo rand(0,3);*/ echo 'NA'?>">
                <input type="text" name="amount-flexibilite[<?php echo $flexibilite['id'] ?>]" id="amount-flexibilite-<?php echo $flexibilite['id'] ?>" readonly class="inputformulaire" value="<?php /*echo rand(0,3);*/ echo 'NA'?>">
            </div>
            <?php
        }
        ?>
	</fieldset>
	<div class="textcenter"><br><br><br><input type="button" name="" value="Valider le formulaire" onclick="envoi()"><br><br></div>
</form>

<script>
	$( function() {
		<?php 
		foreach($intitules as $key=>$intitule)
		{
			?>
			$( "#slider-range-max-<?php echo $intitule['id'] ?>" ).slider({
				range: "max",
				min: 0,
				max: 3,
				value: 0,
				start:function(evente,ui){
					$("#slider-range-max-<?php echo $intitule['id']?> > .ui-state-default").addClass('dejafait');

						$( "#amount-<?php echo $intitule['id'] ?>" ).val( ui.value );
					},
					slide: function( event, ui ) {
						$( "#amount-<?php echo $intitule['id'] ?>" ).val( ui.value );
					}
				});
			<?php
		}
		?>
        <?php
        foreach($empathies as $key=>$empathie)
        {
        ?>
        $( "#slider-range-max-empathie-<?php echo $empathie['id'] ?>" ).slider({
            range: "max",
            min: 1,
            max: 4,
            value: 1,
            start:function(evente,ui){
                $("#slider-range-max-empathie-<?php echo $empathie['id']?> > .ui-state-default").addClass('dejafait');

                var maps = new Map();
                maps.set(1, 4);   // a string key
                maps.set(2, 3);   // a string key
                maps.set(3, 2);   // a string key
                maps.set(4, 1);   // a string key

                var idpos =<?php echo $empathie['id'] ?>;
                if(+idpos > 3){
                    document.getElementById( "amount-empathie-calcule-[<?php echo $empathie['id'] ?>]" ).value = maps.get(ui.value);
                }else {
                    document.getElementById( "amount-empathie-calcule-[<?php echo $empathie['id'] ?>]" ).value = ui.value;
                }

                $("#amount-empathie-<?php echo $empathie['id'] ?>").val(ui.value);

            },
            slide: function( event, ui ) {

                var map = new Map();
                map.set(1, 4);   // a string key
                map.set(2, 3);   // a string key
                map.set(3, 2);   // a string key
                map.set(4, 1);   // a string key

                var idpos =<?php echo $empathie['id'] ?>;
                if(+idpos > 3){
                    document.getElementById( "amount-empathie-calcule-[<?php echo $empathie['id'] ?>]" ).value = map.get(ui.value);
                }else {
                    document.getElementById( "amount-empathie-calcule-[<?php echo $empathie['id'] ?>]" ).value = ui.value;
               }

                $("#amount-empathie-<?php echo $empathie['id'] ?>").val(ui.value);

            }
        });
        <?php
        }
        ?>
        <?php
        foreach($flexibilites as $key=>$flexibilite)
        {
        ?>
        $( "#slider-range-max-flexibilite-<?php echo $flexibilite['id'] ?>" ).slider({
            range: "max",
            min: 1,
            max: 4,
            value: 1,
            start:function(evente,ui){
                $("#slider-range-max-flexibilite-<?php echo $flexibilite['id']?> > .ui-state-default").addClass('dejafait');

                var maps = new Map();
                maps.set(1, 4);   // a string key
                maps.set(2, 3);   // a string key
                maps.set(3, 2);   // a string key
                maps.set(4, 1);   // a string key

                var idpos =<?php echo $flexibilite['id'] ?>;
                if(+idpos > 3){
                    document.getElementById( "amount-flexibilite-calcule-[<?php echo $flexibilite['id'] ?>]" ).value = maps.get(ui.value);
                }else {
                    document.getElementById( "amount-flexibilite-calcule-[<?php echo $flexibilite['id'] ?>]" ).value = ui.value;
                }

                $("#amount-flexibilite-<?php echo $flexibilite['id'] ?>").val(ui.value);

            },
            slide: function( event, ui ) {

                var map = new Map();
                map.set(1, 4);   // a string key
                map.set(2, 3);   // a string key
                map.set(3, 2);   // a string key
                map.set(4, 1);   // a string key

                var idpos =<?php echo $flexibilite['id'] ?>;
                if(+idpos > 3){
                    document.getElementById( "amount-flexibilite-calcule-[<?php echo $flexibilite['id'] ?>]" ).value = map.get(ui.value);
                }else {
                    document.getElementById( "amount-flexibilite-calcule-[<?php echo $flexibilite['id'] ?>]" ).value = ui.value;
                }

                $("#amount-flexibilite-<?php echo $flexibilite['id'] ?>").val(ui.value);

            }
        });
        <?php
        }
        ?>

	} );


	function envoi()
	{
		erreur = 0;
		$.each($(".inputformulaire"),function(){
			
			if($(this).val()=='NA')
				erreur=1;
		})
		if(erreur==0)
			document.getElementById("myForm").submit(); 
		else
			alert('Merci de renseigner tous les champs encore en rouge')
		

	}
</script>