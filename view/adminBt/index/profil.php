<div class="textcenter">
	<a href="<?php echo RACINE?>adminBt"><br><img src="<?php echo RACINE?>img/public/airbus_logo.png"></a><br><br>
</div>
<?php 
$i=0;
$centre_X =588;
$centre_Y = 284;
$taille_grille_x = 1047;
$taille_grille_y= 455;
$point_principal_x = ($profil->coord_x*($taille_grille_x/2))/1.5;
$point_principal_y = -($profil->coord_y*($taille_grille_y/2))/1.5;
$point_principal_x -= 7;
$point_principal_y -= 26 ;

?>
<div class="textcenter titre_profil">
	Profil de <?php echo $profil->nomprenom ?><br>
	<?php //echo 'X : '.$profil->coord_x ?> 
	<?php //echo 'Y : '.$profil->coord_y ?>
</div>

<div class="listeprofil">
LISTE DES MARQUEURS : <br>
	<?php 
	echo '<b>0</b> : '.$profil->nomprenom.' / ';
	$j=0;
	foreach($profils_additionnels as $key=>$profil_additionnel){
		$j++;
		echo '<b>'.$j.'</b> : '.$profil_additionnel->nomprenom.' / ';
	}

	?>
</div>


<div class="grille">
	<div class="positionnement_grille" style="top:<?php echo $centre_Y?>px;left:<?php echo $centre_X?>px;">
		<!-- ON PLACE LE POINT PRINCIPAL DE LA PERSONNE DE BASE-->
		<div id="point<?php echo $i ?>" class="point point_principal point_actif" style="top:<?php echo $point_principal_y ?>px; left:<?php echo $point_principal_x ?>px" onclick="clic_pins('<?php echo $i ?>')" title="<?php echo $profil->nomprenom ?>" alt="<?php echo $profil->nomprenom ?>"><?php echo $i?></div>

		<!-- ON PLACE TOUS LES POINTS PROFIL ADDITIONNELS-->
		<?php 
		foreach($profils_additionnels as $key=>$profil_additionnel){
			$i++;
			$point_additionnel_x = ($profil_additionnel->coord_x*($taille_grille_x/2))/1.5;
			$point_additionnel_y = -($profil_additionnel->coord_y*($taille_grille_y/2))/1.5;
			$point_additionnel_x -= 7;
			$point_additionnel_y -= 26 ;
			echo '<div id="point'.$i.'" class="point point_additionnel" style="top:'.$point_additionnel_y.'px; left:'.$point_additionnel_x.'px" onclick="clic_pins(\''.$i.'\')" alt="'.$profil_additionnel->nomprenom.'" title="'.$profil_additionnel->nomprenom.'">'.$i.'</div>';
		} ?>
	</div>
</div>

<br/>
<table class="table">
    <tr>
        <td><h3>0</h3></td>
        <td> <h3>Niveau d'empathie: <?php echo $profil->flexibilite_moy ?></h3></td>
        <td><h3> Niveau de flexibilité: <?php echo $profil->empathie_moy ?></h3></td>
    </tr>
<?php
foreach($profils_additionnels as $key=>$profil_additionnel){
    echo '
    <tr>
        <td><h3>'.$i.'</h3></td>
        <td><h3>Niveau de flexibilité:'.$profil_additionnel->flexibilite_moy . '</h3></td>
        <td><h3>    Niveau d\'empathie: '.$profil_additionnel->empathie_moy.'</h3></td>

    </tr>

';
    $i++;

} ?>
</table>

<br/>
<br>







<?php $i=0; ?>
<div id="tabs">
	<!-- MENU NAVIGATION TABS-->
	<ul>
		<li><a href="#tabs-<?php echo $i ?>">Profil  de <?php echo $profil->nomprenom ?></a></li>
		<?php 
		foreach($profils_additionnels as $key=>$profil_additionnel){
			$i++;
			echo '<li><a href="#tabs-'.$i.'">Vu par '.$profil_additionnel->nomprenom.'</a></li>';

		}
		?>
	</ul>
	<?php $i=0; ?>
	<!-- PROFIL PRINCIPAL -->
	<div id="tabs-<?php echo $i ?>">
		<?php 
		foreach($intitules as $key=>$intitule)
		{
			?>
			<div class="question">
				<div class="libelle">
					<div class="cas1"><?php echo $intitule['gauche_fr'] ?></div>
					<div class="cas2"><?php echo $intitule['droite_fr'] ?></div>
				</div>
				<div class="slider" id="slider-range-max-<?php echo $intitule['id'] ?>"></div>
			</div>
			<?php
		}
		?>
        <?php
        foreach($empathies as $key=>$empathie)
        {
            ?>
            <div class="question">
                <div class="libelle">
                    <div class="cas1"><?php echo $empathie['gauche_fr'] ?></div>
                    <div class="cas2"><?php echo $empathie['droite_fr'] ?></div>
                </div>
                <div class="slider" id="slider-range-max-empathie<?php echo $empathie['id'] ?>"></div>
            </div>
            <?php
        }
        ?>
		<script>
			$( function() {
				<?php 
				$grille_reponses = unserialize($profil->grille_reponses);
				foreach($intitules as $key=>$intitule)
				{
					?>
					$( "#slider-range-max-<?php echo $intitule['id'] ?>" ).slider({
						range: "max",
						min: 0,
						max: 3,
						value:<?php echo $grille_reponses[$intitule['id']]  ?>,
						slide: function( event, ui ) {
							return false;
						}
					});
					<?php
				}
				?>
                <?php
                $grille_reponse_empathie = unserialize($profil->grille_reponse_empathie);
                foreach($empathies as $key=>$empathie)
                {
                ?>
                $( "#slider-range-max-empathie<?php echo $empathie['id'] ?>" ).slider({
                    range: "max",
                    min: 1,
                    max: 4,
                    value:<?php echo $grille_reponse_empathie[$empathie['id']]  ?>,
                    slide: function( event, ui ) {
                        return false;
                    }
                });
                <?php
                }
                ?>
			} );
		</script>
		<div class="clearfix"></div>
	</div>


	<!-- PROFIL ADDITIONNELS -->
	<?php 
	foreach($profils_additionnels as $key=>$profil_additionnel){
		$i++;
		?>
		<div id="tabs-<?php echo $i ?>">
			<?php 
			foreach($intitules as $key=>$intitule)
			{
				?>
				<div class="question">
					<div class="libelle">
						<div class="cas1"><?php echo $intitule['gauche_fr'] ?></div>
						<div class="cas2"><?php echo $intitule['droite_fr'] ?></div>
					</div>
					<div class="slider" id="slider-range-max-<?php echo $profil_additionnel->id ?>-<?php echo $intitule['id'] ?>"></div>
				</div>
				<?php
			}
			?>
			<script>
				$( function() {
					<?php 
					$grille_reponses = unserialize($profil_additionnel->grille_reponses);
					foreach($intitules as $key=>$intitule)
					{
						?>
						$( "#slider-range-max-<?php echo $profil_additionnel->id ?>-<?php echo $intitule['id'] ?>").slider({
							range: "max",
							min: 0,
							max: 3,
							value:<?php echo $grille_reponses[$intitule['id']]  ?>,
							slide: function( event, ui ) {
								return false;
							}
						});
						<?php
					}
					?>  
				} );
			</script>
			<div class="clearfix"></div>
		</div>
		<?php
	}
	?> 
</div>

<br><br>


<script>

	$( "#tabs" ).tabs({
		activate: function( event, ui ) {
			var active = $( "#tabs" ).tabs( "option", "active" );
			$(".point").removeClass('point_actif');
			$("#point"+active).addClass('point_actif');
		}
	});
	//$( "#tabs").tabs( "option", "active", 1 );

	function clic_pins(index){
		$( "#tabs").tabs( "option", "active",index );
	}
	
</script>  