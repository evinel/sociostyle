<div class="textcenter">
	<br><img src="<?php echo RACINE?>img/public/airbus_logo.png"><br><br>
</div>

<table class="table table-hover table-bordered table-striped">
	<thead>
		<tr>
			<th>Session du</th>
			<th>Participant</th>
			<th>Profil</th>
			<th>Code partage</th>
			<th>Supprimer</th>
		</tr>
	</thead>
	<?php 
	foreach($profils as $key=>$profil){
		?>
		<tr>
			<td><?php echo $profil->date_reponse ?></td>
			<td><?php echo $profil->nomprenom ?></td>
			<td><a href="<?php echo RACINE.'adminBt/profil/'.$profil->id ?>"><img src="<?php echo RACINE?>img/admin/details.png"></a></td>
			<td><a href="<?php echo RACINE.'?code_secret='.$profil->code_secret ?>" target="_blank"><?php echo $profil->code_secret ?></a></td>
			<td><a href="<?php echo RACINE.'adminBt/'.$profil->code_secret ?>/delete">Supprimer</a></td>
		</tr>
		<?php 
	}
	?>	