<?php
error_reporting(0);

class Dispatcher {
    var $request;

    function __construct() {
        $this->request = new Request(); // création de l'objet Request qui récupère l'URL saisien
        
        // création et initialisation du controleur de traduction
        $this->i18n = new i18n();
        $this->i18n->init(); 
        
        Router::parse($this->request->url, $this->request); // on parse l'URL grace au routeur et à l'URL saisie -> on connnait donc controller, action et parametres saisi en URL
        $controller = $this->loadController(); // on va passer maintenant les infos au controleur
        
        /* on verifie si la methode (action) demandée existe dans le controller de l'action  */
        if (!in_array($this->request->action, get_class_methods($controller))) {
            $this->errorDispatcher('Le controlleur "' . $this->request->controller . '" n\'a pas de méthode "' . $this->request->action . '"');
        }
        /* on appelle la fonction "action" du controller demandé en URL (controller/action/params) et en second argument les paramètres */
        call_user_func_array(array($controller, $this->request->action), $this->request->params);
        $controller->render($this->request->action); // on fait le rendu de l'action exemple : rendu de la vue par défaut : view/Controller/index.php

    }

    /**
     * Affichage des erreurs
     */
    function errorDispatcher($message) {
        header("HTTP/1.0 404 Not Found");
        $controller = new Controller($this->request);
        $controller->setParamsView('message', $message);
        $controller->render('/errors/404');
        die();
    }

    function loadController() {
        if ($this->request->controller)
            $name = ucfirst($this->request->controller) . 'Controller';
        
        if($this->request->repertoire) 
            $rep = DS.ucfirst($this->request->repertoire);
        else
            $rep='';

        /* on inclus maintenant ce fichier pour pouvoir s'en servir */
       //on teste si ce fichier Controller existe bien 
        // si c'est le cas on l'inclu sinon on envoie une erreur   
        
        $file = ROOT . DS . 'controller' .$rep. DS . $name . '.php';

        if (file_exists($file))
            require($file);
        else
            $this->errorDispatcher('Le controlleur "' . $this->request->controller . '" n\'existe pas');
        
        /* on instancie un nouvel objet Controller avec le "name" reconstitué par l'URL retournée par le Routeur et on lui passe en paramètres l'objet request et on le retourne */
        return new $name($this->request);
    }






}
?>