<?php
error_reporting(0);

function debug($r) {
	if(Conf::$debug > 0)
	{
		$backtrace = debug_backtrace();
		echo "<pre>";
		echo '<p>Ligne '.$backtrace[0]['line'].' --> '.$backtrace[0]['file'].'</p><p>-----------------</p>';
		print_r($r);
		echo "</pre>";
	}
}


function SecureEnv() {
	if(isset($_POST))
		{$_POST = SecureFlux($_POST);}
	if(isset($_GET))
		$_GET = SecureFlux($_GET);
}


function SecureFlux($traitement)
{
	$traitement=array_map('removeTag', $traitement);
	return($traitement);
}

function removeTag($chaine) {

	if(!is_array($chaine))
	{
		$patterns = $replacements = array(); // on rajoute 'i' dans tous les patterns pour case insensitive
		$patterns[0] = '/script/i';   			$replacements[0] = '';
		$patterns[1] = '/onload/i';   			$replacements[1] = '';
		$patterns[2] = '/iframe/i';   			$replacements[2] = '';
		//$patterns[3] = '/"/i';   				$replacements[3] = '';
		return preg_replace($patterns,$replacements,$chaine);
	}
	else
		return $chaine;
}





function tronquer ($str, $size) {
	// suppression du code HTML
	$str = preg_replace('/<[^>]*>/', '', $str);
	if(strlen($str) >= $size) {
		$str = substr($str, 0, $size);
		$espace = strrpos($str, ' ');
		if($espace) $str = substr($str, 0, $espace);
		$str .= '...';
	}
	// suppression des espaces inutiles
	$str = preg_replace('/&nbsp;/', ' ', $str);
	$str = trim($str);
	$str = str_replace("\t", ' ', $str);
	$str = preg_replace('/[ ]+/', ' ', $str);
	return $str;
}

function alert ($msg) { echo '<script type="text/javascript">alert("'.$msg.'");</script>'; }
function redirigeJS ($url) { echo '<script type="text/javascript">window.location.replace("'.$url.'");</script>'; exit(); }
function redirigePHP ($url) { header("Location:".$url); exit(); }


function expreg($traitement)
{
	$pattern1="[éèêë]";
	$pattern2="[à]";
	$pattern3="[üù]";
	$pattern4="[']";
	$pattern5="[,]";
	$pattern6="[()]";
	$pattern7="[ôö]";
	$traitement=preg_replace("/$pattern1/","e",$traitement);
	$traitement=preg_replace("/$pattern2/","a",$traitement);
	$traitement=preg_replace("/$pattern3/","u",$traitement);
	$traitement=preg_replace("/$pattern4/","-",$traitement);
	$traitement=preg_replace("/$pattern5/","-",$traitement);
	$traitement=preg_replace("/$pattern6/","-",$traitement);
	$traitement=preg_replace("/$pattern7/","o",$traitement);
	$traitement=preg_replace("/ /","-",$traitement);
	$traitement=strtolower($traitement);

	return($traitement);
}

?>
