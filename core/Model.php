<?php
error_reporting(0);

class Model{
	
	static $connections = array();
	public $conf = 'default';
	public $db;

 	
	/**
	 * Connexion à la BDD
	 */
	public function __construct(){
		// je me connecte à ma DB
        $confobj = new Conf();

		if(isset(Model::$connections[$this->conf])){
			$this->db = Model::$connections[$this->conf];
			return true;
		}

        try {

           $conStr = sprintf("pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s",
               $confobj->database['host'],
               $confobj->database['port'],
               ltrim( $confobj->database['path'],"/"),
               $confobj->database['user'],
               $confobj->database['pass']);

            $pdo = new \PDO($conStr);
          //  $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );


            Model::$connections[$this->conf] = $pdo;
			$this->db = $pdo;
			}
		catch(PDOException $e){
			if(Conf::$debug >= 1)
				exit($e->getMessage());
			else
				exit('Impossible de se connecter à la base de données');
			}
	}


	/**
	 * Fonction de insert/update
	 * @param $data : la variable postée dans le formulaire
	 * @param $table : nom de la table sur laquelle je vais faire la requete
	 * @param $id=null : numéro de l'enregistrement à mettre à jour, si null alors INSERT
     * @return true ou false
	 */
	public function saveBdd($data,$table,$id=null){
		$retour_fonction = "nbr_lignes_affectees";
        $query= "SELECT column_name as \"Field\", data_type AS \"Type\", is_nullable AS \"Null\", CASE WHEN constraint_type='PRIMARY KEY' THEN 'PRI' ELSE NULL END AS \"Key\", column_default AS \"Default\", CASE WHEN column_default LIKE 'nextval(%' THEN 'auto_increment' ELSE '' END AS \"Extra\" FROM table_column_constraints as given WHERE given.table_name = '$table'  AND NOT EXISTS (SELECT * FROM table_column_constraints other WHERE other.column_name=given.column_name AND given.constraint_type!='PRIMARY KEY' AND other.constraint_type='PRIMARY KEY');";





		// on attrape les clés de la table $table

        $pre = $this->db->prepare($query);
		$pre->execute();
		$result['champs']= $pre->fetchAll(PDO::FETCH_ASSOC);
		$cles = array();
		$bind = array();
		
		foreach ($result['champs'] as $key => $value) {
			array_push($cles, $value['Field']);
			if($value['Key']=="PRI")
				$primaryKey=$value['Field'];
		}

		// si ID renseigne alors on UPDATE
		if($id)
			{
				$query = "UPDATE $table SET ";
				foreach ($data as $key => $value)
					{
						if(in_array($key,$cles))
							{
								$query.="$key = ? ,";
								array_push($bind, $value);
							}
					}
				$query = trim($query,',');
				$query .= " WHERE $primaryKey=$id";
			}

		// sinon on INSERT	
		else
			{
				// on regle le retour pour avoir le derniere index recuperable
				$retour_fonction = "dernier_index_ajoute";
				$query = "INSERT INTO $table (";
				foreach ($data as $key => $value)
					{
						if(in_array($key,$cles))
							$query.="$key,";
					}
				$query = trim($query,',');
				$query .= ") VALUES (";
				foreach ($data as $key => $value)
					{
						if(in_array($key,$cles))
							{
								$query.="? ,";
								array_push($bind, $value);
							}
					}
				$query = trim($query,',');
				$query .=")";
			}

        $pre = $this->db->prepare($query);



        if($retour_fonction=='dernier_index_ajoute')
			{
				$pre->execute($bind);
				return $this->db->lastInsertId();
			}
		else
				return $pre->execute($bind);
	}

	/**
	 * Permet de supprimer d'une table un enregistrement
	* @param : $table = nom de la table
	* @param : $id = numéro de l'id de  la clé primaire
	 */
	public function deleteBdd($table,$id){
		// on attrape la clé primaire de la table $table
		$query=" SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'";
		$pre = $this->db->prepare($query);
		$pre->execute();
		$result= $pre->fetch(PDO::FETCH_OBJ);
		// on supprime la ligne avec la bonne clé primaire
		$query="delete from $table where $result->Column_name = :id";
		$pre = $this->db->prepare($query);
		$pre->execute(array(':id'=>$id));		
		return $pre->rowCount();
	}


}
?>