<?php
error_reporting(0);

class Controller {

    private $rendered = false; // variable qui va servir à savoir si la vue a déjà été rendu ou pas
    private $vars = array(); // tableau de paramètres passé à ma vue (private 'vars' utilisée nulle part ailleurs)
    public $request; // URL parsée
    public $layout = 'defaultLayout'; // layout général de ma page principale
    public $addCss = ''; // variable qui va permettre de rajouter des Css dans la vue
    public $addJs = ''; // variable qui va permettre de rajouter des JS dans la vue

    /**
     * Constructeur
     * récupere la requête du client dans l'URL
     * @param type $request
     */

    function __construct($request=null) {
        $this->request = $request; // on stocke la requete demandée en URL (initialisée dans le construct du  DIspatcher)
        $this->setParamsView($result);

        //$this->setParamsView($this->request->params);
    }

    /**
     * Affichage des erreurs
     */
    function error($message=null) {
        header("HTTP/1.0 404 Not Found");
        $this->setParamsView('message_erreur', $message);
        $this->render('/errors/404');
        die();

    }

    /**
     * Permet de demander une vue qui sera envoyée au client
     * @param $view : nom de la page PHP utilisée pour la vue (sans le.php)
     * @return
     * */
    public function render($view) {

        // on envoie des CSS supplémentaires à la vue le cas échéant
        $this->setParamsView('addCss',$this->addCss);
        // on envoie des JS supplémentaires à la vue le cas échéant
        $this->setParamsView('addJs',$this->addJs);

        //si la vue a déjà été générée on retourne false (cas typique d'un render dans une méthode d'un controleur)
        if ($this->rendered) {
            return false;
        }


        if($this->request->repertoire)
           $rep = DS.$this->request->repertoire; // $rep = DS.ucfirst($this->request->repertoire);
       else
        $rep='';

        // on teste si on passe en vue un slash en première position, afin d'appeler une page hors du controller directement
        // exemple dans une fonction d'un controller on ferait  $this->render('/errors/404'); -> appel de la vue 404.php du dossier view/errors
    if (strpos($view, '/') === 0) {
        $view = ROOT . DS . 'view' . DS .$view . '.php';
    } else {
        $view = ROOT . DS . 'view' . $rep .DS . $this->request->controller . DS . $view . '.php';
    }


        extract($this->vars); // on éclate les variable dans le tableau "vars" pour pouvoir s'en servir dans la vue
		ob_start(); // demarre la temporisation de la sortie, à partir de là rien ne sera affiché tant que je n'aurai pas donné l'ordre de le faire
        require ($view);
        $content_for_layout = ob_get_clean(); // tout ce qui était censé s'afficher jusqu'à maintenant passe dans la variable $content_for_layout
        // au cas ou on a un ckeditor, alors on nettoie les balises images avant de rendre la vue
        $content_for_layout = $this->nettoieImgCkeditor($content_for_layout);
        require ROOT . DS . 'view' . DS . 'layout' . DS . $this->layout . '.php'; // j'inclue le layout principal
        $this->rendered = true;
    }

    /**
     * Permet de remplir un tableau de valeur qui seront utilisables dans  ma vue
     * @param $key : nom de la variable OU tableau de variables
     * @param $value : valeur de cette clé (Optionnel)
     * */
    public function setParamsView($key, $value = null) {

        // cas où je reçois un tableau de paramètres
        if (is_array($key)) {
            $this->vars += $key;
        }
        // cas ou je reçois juste un seul paramètre
        else {
            $this->vars[$key] = $value;
        }        
    }

    /**
     *  permet de charger un modèle d'accès à la BDD
     * @param type $name
     */
    function loadModel($name) {
        $file = ROOT . DS . 'model' . DS . $name . '.php';
        require_once($file);
        // si on ne s'est pas déjà servi du modèle alors on crée l'objet (pas la peine de le créer 2 fois)
        if (!isset($this->$name)) {
            $this->$name = new $name(); // si $name vaut post, ça revient à écrire : $this->Post = new Post();
        }
    }


    /**
     *  permet de charger un SERVICE sans le construire
     * @param type $name
     */
    function loadService($name) {
        $file = ROOT . DS . 'service' . DS . $name . '.php';
        require_once($file);
    }

     /**
     *  permet de charger un SERVICE et de le construire
     * @param type $name
     */
     function loadServiceAndConstruct($name) {
        $file = ROOT . DS . 'service' . DS . $name . '.php';
        require_once($file);
        // si on ne s'est pas déjà servi du modèle alors on crée l'objet (pas la peine de le créer 2 fois)
        if (!isset($this->$name)) {
            $this->$name = new $name(); // si $name vaut post, ça revient à écrire : $this->Post = new Post();
        }
    }




    /**
     * controle de la personne loggée à l'admin
     */
    public function isLogged(){

        if(Conf::$sso){
            $this->loadModel('apiRequest');
            $user = $this->apiRequest->getRequest(session_id());
            if($user->user_info != ""){
                $this->apiRequest->removeRequest(session_id());
                $_SESSION['appAuth'] = session_id();
                $_SESSION['appUser'] = json_decode($user->user_info);
            }
            if(!isset($_SESSION['appAuth']) || $_SESSION['appAuth']!=session_id()){
              $url = RACINESSO.'api/register';
              $this->apiRequest->addRequest(session_id(),$_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],"");
              $data = array('idSession' => session_id(), 'idApplication' => ID_APP,'reponse' => RACINE."api/userInfo", 'request' => $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"], 'md' => "W@p321c1sc0");

              $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                    )
                );
              $context  = stream_context_create($options);
              $result = file_get_contents($url, false, $context);
              if ($result === FALSE) {  }

                  redirigePHP(RACINESSO."api/auth/".session_id());
          }
      }
      else
      {
         if((!isset($_SESSION['auth']) || ($_SESSION['auth']!=session_id())))
            redirigePHP(RACINE."adminBt/login");
      }
  }




    /**
     * logOut
     */
    public function logOut()
    {
        session_destroy();
        redirigePHP(RACINE."adminBt/login");
    }

    /**
     *  permet de charger un modèle CSS supplémentaire
     * @param type $name -> nom du chemin vers le fichier CSS
     */
    function loadCss($name) {
      if(preg_match("/http:\/\//", $name))
        $this->addCss .= '<link href="'.$name.'" rel="stylesheet" type="text/css" media="all"  />';
    else
        $this->addCss .= '<link href="'.RACINE.'css/'.$name.'.css" rel="stylesheet" type="text/css" media="all"  />';
}

    /**
     *  permet de charger un modèle JS supplémentaire
     * @param type $name -> nom du chemin vers le dossier fichier JS
     */
    function loadJs($name) {
        // exeption pour ckeditor ou il faut que je récupère la racine CSS pour la config du fichier config.js
        if($name == 'ckeditor/ckeditor')
            $this->addJs .= "<script>racinecss = '".RACINE."'</script>";

        if(preg_match("/http:\/\//", $name))
            $this->addJs .= '<script type="text/javascript" src="'.$name.'"></script>';
        else
            $this->addJs .= '<script type="text/javascript" src="'.RACINE.'js/'.$name.'.js"></script>';
    }

    /**
     * Permet de dégager les mauvais caractères SRC IMG des contenus enregistrés par CKEditor
     */
    function nettoieImgCkeditor($chaine){
        $chaine=str_replace(BASE_URL."/webroot/js/ckeditor/elfinder/php/../../../../",RACINE,$chaine);
        return $chaine;
    }

    /**
    * permet d'appeler un controleur depuis une vue
    **/
    public function controllerFromView($repertoire=null,$controller,$action){

        $controller .= 'Controller';
        if($repertoire)
            $repertoire=DS.$repertoire;

        $file = ROOT.DS.'controller'.$repertoire.DS.$controller.'.php';
        if (file_exists($file))
            require_once($file);
        else
            $this->error('Le controlleur "' . $controller . '" n\'existe pas');

        $c = new $controller();
        return $c->$action();
    }









}

?>
