<?php
error_reporting(0);

/* Permet d'analyser l'URL et passer les infos récoltées au Dispatcher */
class Request{
	
	public $url; /* servira à stocker l'URL saisie par l'utilisateur pour ensuite la traiter */
	
	function __construct(){
		$path_info = $_SERVER['REQUEST_URI'];

		if(!empty($path_info))
			$this->url = $path_info;
		else
			$this->url="index";
		/*
		if(isset($_SERVER['PATH_INFO']))
			$this->url = $_SERVER['PATH_INFO']; 
		else
		$this->url="index";
		*/
		
	}
	
}
?>