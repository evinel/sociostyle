<?php
error_reporting(0);

/* Permet de parser les URL et passer ça à Request */
class Router {

  /** Fonction statique --> pourra être appelée en faisant Router::fonction 
  * permet de parser une URL
  * @param $url : url à parser
  * @return : tableau contenant les paramètres (controller,action,params[])
  */
  static $maRoute = array();

  
  static function parse($url,$request) {
    // vire les slash en début et en fin d'URL
    $url = trim($url,'/'); 

    Router::getRoutes($url);

    $request->repertoire = isset(Router::$maRoute['repertoire']) ? Router::$maRoute['repertoire'] : '' ;
    $request->controller = isset(Router::$maRoute['controller']) ? Router::$maRoute['controller'] : 'index';
    $request->action = isset(Router::$maRoute['action']) ? Router::$maRoute['action'] : 'index';
    $request->params = isset(Router::$maRoute['vars']) ? Router::$maRoute['vars'] : array();

      // je vire les magic quotes 
    Router::removeMagicQuotes();
      // recup des POST
    $request->params = array_merge($_POST,$request->params);
      // recup des GET
    $request->params = array_merge($_GET,$request->params);

      // à ce niveau là, l'objet $request à récupéré tous les paramètres de l'URL
    return true;      
  }

  /**
  * Fonction qui permet de récupérer les routes du fichier config/routes.xml
  */
  static function getRoutes($urlNav) {      
    $xml = new DOMDocument;
    $xml->load(ROOT.DS.'config'.DS.'routes.xml');
    $routes = $xml->getElementsByTagName('route');

    foreach ($routes as $route) {
        //on protège les slashs dans l'URL
      $routeModif = str_replace('/', '\/',$route->getAttribute('url')); 
       // on implémente le pattern
      $pattern = '/'.$routeModif.'/'; 

      if(preg_match($pattern,$urlNav,$matches)) {
        $matches = array_slice($matches,1);
        $params = array(); 
        if ($route->hasAttribute('vars')) {
          $vars = explode (",",$route->getAttribute('vars'));

          foreach ($vars as $key => $value)  
            $params[$value] = $matches[$key]; 
        }
        
        Router::$maRoute = array('controller' => $route->getAttribute('controller'),
          'action'  => $route->getAttribute('action'),
          'repertoire'  => $route->getAttribute('repertoire'),
          'vars' => $params);
        return true;
      }
    }
  }





  private static function stripSlashesDeep($value) {
    if(is_array($value)){
      foreach($value as $key=>$val) 
        $value[$key] = Router::stripSlashesDeep($val);  
    }
    else 
      $value = stripslashes($value);

    return $value;
  }

  private static function removeMagicQuotes() {
    if (get_magic_quotes_gpc()) {
      $_GET    = Router::stripSlashesDeep($_GET);
      $_POST   = Router::stripSlashesDeep($_POST);

    }
  }

}

?>