<?php class L {
const greeting = 'Félicitations';
const welcome = 'Bienvenue';
const category°somethingother = 'quelque chose d\'autre';
const unesecondecategory°unautretag = 'voici un autre tag';
public static function __callStatic($string, $args) {
    return vsprintf(constant("self::" . $string), $args);
}
}
function L($string, $args=NULL) {
    $return = constant("L::".$string);
    return $args ? vsprintf($return,$args) : $return;
}