
CREATE TABLE IF NOT EXISTS intitules (
  id int NOT NULL,
  gauche_fr varchar(255) NOT NULL,
  droite_fr varchar(255) NOT NULL,
  gauche_en varchar(255) NOT NULL,
  droite_en varchar(255) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT id UNIQUE  (id)
) ;



CREATE SEQUENCE profils_seq;

CREATE TABLE IF NOT EXISTS profils (
  id bigint NOT NULL DEFAULT NEXTVAL ('profils_seq'),
  code_secret varchar(255) NOT NULL,
  profil_additionnel varchar(255) NOT NULL,
  nomprenom varchar(255) NOT NULL,
  date_reponse timestamp(0) NOT NULL,
  grille_reponses text NOT NULL,
  coord_x text NOT NULL,
  coord_y text NOT NULL,
  PRIMARY KEY (id)
)   ;



CREATE SEQUENCE users_seq;

CREATE TABLE IF NOT EXISTS users (
  id bigint NOT NULL DEFAULT NEXTVAL ('users_seq'),
  id_entreprise bigint NOT NULL,
  email varchar(100) NOT NULL,
  password varchar(250) NOT NULL,
  nom varchar(255) NOT NULL,
  droits varchar(5) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT email UNIQUE  (email)
)   ;


INSERT INTO users (id, id_entreprise, email, password, nom, droits) VALUES
(1, 1, 'stephane.neuzillet@airbus.com', '0261fa9fcaf69507555899cded5435d8f6961ede8ec034e23372c1b0780903ce', 'NEUZILLET Stéphane', 'admin');


INSERT INTO intitules (id, gauche_fr, droite_fr, gauche_en, droite_en) VALUES
(1, 'Sûr de soi', 'Hésitant', 'Sûr de soi', 'Hésitant'),
(2, 'Prend l''initiative', 'Recherche le conseil', 'Prend l''initiative', 'Recherche le conseil'),
(3, 'S''impose', 'S''efface', 'S''impose', 'S''efface'),
(4, 'Déclenche', 'Met en oeuvre, élabore', 'Déclenche', 'Met en oeuvre, élabore'),
(5, 'Affirme', 'Suggère', 'Affirme', 'Suggère'),
(6, 'Veut aller vite', 'Prend le temps', 'Veut aller vite', 'Prend le temps'),
(7, 'Direct', 'Nuancé', 'Direct', 'Nuancé'),
(8, 'Prend des risques', 'Prend du recul', 'Prend des risques', 'Prend du recul'),
(9, 'Ordonne', 'Consulte', 'Ordonne', 'Consulte'),
(10, 'Reste sur ses positions', 'Est prêt à revoir ses positions', 'Reste sur ses positions', 'Est prêt à revoir ses positions'),
(11, 'Réfléchi', 'Impulsif', 'Réfléchi', 'Impulsif'),
(12, 'Logique', 'Affectif', 'Logique', 'Affectif'),
(13, 'Parle peu', 'Parle beaucoup', 'Parle peu', 'Parle beaucoup'),
(14, 'Sérieux', 'Jovial', 'Sérieux', 'Jovial'),
(15, 'Très organisé', 'Peu organisé', 'Très organisé', 'Peu organisé'),
(16, 'Se confie peu', 'Se confie facilement', 'Se confie peu', 'Se confie facilement'),
(17, 'Comportement strict', 'Comportement décontracté', 'Comportement strict', 'Comportement décontracté'),
(18, 'Réservé', 'Enthousiaste', 'Réservé', 'Enthousiaste'),
(19, 'Difficile à émouvoir', 'Facile à émouvoir', 'Difficile à émouvoir', 'Facile à émouvoir'),
(20, 'Ne pardonne pas facilement', 'Pardonne facilement', 'Ne pardonne pas facilement', 'Pardonne facilement'),
(21, 'Téméraire', 'Prudent', 'Téméraire', 'Prudent'),
(22, 'Intuitif', 'Raisonné', 'Intuitif', 'Raisonné'),
(23, 'Fait confiance', 'Se méfie', 'Fait confiance', 'Se méfie'),
(24, 'Porte des jugements', 'Reste neutre', 'Porte des jugements', 'Reste neutre'),
(25, 'Insouciant', 'Anxieux', 'Insouciant', 'Anxieux'),
(26, 'Apprécie le mouvement', 'Apprécie la stabilité', 'Apprécie le mouvement', 'Apprécie la stabilité'),
(27, 'Aventurier', 'Méthodique', 'Aventurier', 'Méthodique'),
(28, 'Imprécis', 'Précis', 'Imprécis', 'Précis'),
(29, 'S''expose', 'Sécurise sa position', 'S''expose', 'Sécurise sa position'),
(30, 'Exubérant', 'Distant', 'Exubérant', 'Distant'),
(31, 'Affairé', 'Détendu', 'Affairé', 'Détendu'),
(32, 'Agit', 'Est à l''écoute', 'Agit', 'Est à l''écoute'),
(33, 'Intransigeant', 'Conciliant', 'Intransigeant', 'Conciliant'),
(34, 'Tranchant', 'Pondéré', 'Tranchant', 'Pondéré'),
(35, 'Apprécie l''autonomie', 'Apprécie le travail en équipe', 'Apprécie l''autonomie', 'Apprécie le travail en équipe'),
(36, 'Sert d''abord  ses intérêts', 'Pense d''abord aux autres', 'Sert d''abord ses intérêts', 'Pense d''abord aux autres'),
(37, 'Valorise l''efficacité', 'Valorise la convivialité', 'Valorise l''efficacité', 'Valorise la convivialité'),
(38, 'Déterminé', 'Accommodant', 'Déterminé', 'Accommodant'),
(39, 'Se fixe des objectifs', 'Prend les choses comme elles viennent ', 'Se fixe des objectifs', 'Prend les choses comme elles viennent '),
(40, 'Décide seul', 'S''en remet à l''opinion de son entourage', 'Décide seul', 'S''en remet à l''opinion de son entourage');

---
ALTER TABLE profils ADD grille_reponse_empathie TEXT ;
ALTER TABLE profils ADD empathie_moy INT ;

create table empathies
(
    id int not null,
    gauche_fr varchar(255) not null,
    droite_fr varchar(255) not null,
    gauche_en varchar(255) not null,
    droite_en varchar(255) not null
);

create unique index "empathies_id_uindex"
    on empathies (id);

alter table empathies
    add constraint "empathies_pk"
        primary key (id);

INSERT INTO empathies (id, gauche_fr, droite_fr, gauche_en, droite_en) VALUES
(1, 'Ecoute peu', 'A l’écoute', 'Ecoute peu', 'A l’écoute'),
(2,'Sévère' ,'Indulgent','Sévère' ,'Indulgent' ) ,
(3,'Ne se connecte pas avec son interlocuteur' ,'Sait se connecter avec son interlocuteur' ,'Ne se connecte pas avec son interlocuteur' ,'Sait se connecter avec son interlocuteur'     ) ,
(4, 'Serviable', 'Pense d’abord à soi', 'Serviable', 'Pense d’abord à soi'),
(5,'Partage la peine des autres' ,'Ne partage pas la peine des autres','Partage la peine des autres' ,'Ne partage pas la peine des autres' ),
(6,'Prévenant','Egoïste','Prévenant','Egoïste' )  ,
(7,'Se met à la place de l’autre','Reste dans son monde','Se met à la place de l’autre','Reste dans son monde') ,
(8,'Reformule et pose des questions' ,'N''écoute pas activement' ,'Reformule et pose des questions' ,'N''écoute pas activement' ) ,
(9,'Accorde du temps aux autres','N''a pas le temps pour les autres'   ,'Accorde du temps aux autres','N''a pas le temps pour les autres'  ) ;


create table flexibilite
(
    id int not null,
    gauche_fr varchar(255) not null,
    droite_fr varchar(255) not null,
    gauche_en varchar(255) not null,
    droite_en varchar(255) not null
);

create unique index "flexibilite_id_uindex"
    on flexibilite (id);

alter table flexibilite
    add constraint "flexibilite_pk"
        primary key (id);

create table flexibilite
(
    id int not null
        constraint flexibilite_pk
            primary key,
    gauche_fr varchar(255) not null,
    droite_fr varchar(255) not null,
    gauche_en varchar(255) not null,
    droite_en varchar(255) not null
);

INSERT INTO flexibilite (id, gauche_fr, droite_fr, gauche_en, droite_en) VALUES
(1,	'Ferme','Compréhensif','Ferme','Compréhensif') , -- 4 à Droite
(2,'Têtu','Accommodant' ,'Têtu','Accommodant'    ),
(3,	'Rigide','A l’esprit ouvert',	'Rigide','A l’esprit ouvert'),
(4,	'Large d’esprit',' Etroit d’esprit ',      'Large d’esprit',' Etroit d’esprit '),   --                                         4 à Gauche
(5,	'S’adapte facilement','S’adapte difficilement','S’adapte facilement','S’adapte difficilement'),
(6,	'Change facilement d’idée','Change difficilement d’idée','Change facilement d’idée','Change difficilement d’idée'),
(7,	'Flexible','Inflexible','Flexible','Inflexible'),                            --                       4 à Gauche
(8,	'Prêt à revoir ses positions','Reste sur ses positions','Prêt à revoir ses positions','Reste sur ses positions'),
(9,	'Se met en mouvement','reste sur ses acquis','Se met en mouvement','reste sur ses acquis');


ALTER TABLE profils ADD grille_reponse_flexibilite TEXT ;
ALTER TABLE profils ADD flexibilite_moy INT ;