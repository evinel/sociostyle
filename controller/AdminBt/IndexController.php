<?php
error_reporting(0);

class IndexController extends Controller {

	public $layout = 'adminLayout';
	
	/* --------------------------------- Fonctions Admin -------------------------------------- */
	function index() {
		$this->isLogged();
		// traduction
		//$_SESSION['lang']="fr";
		$this->loadModel('profil');
		$results['profils'] = $this->profil->getProfils();
		$this->setparamsView($results);
	}

	function delete() {
		$this->isLogged();
		$this->loadModel('profil');
		$this->profil->deleteProfil($this->request->params['code_secret']);
		redirigePHP(RACINE.'adminBt');
	}

	function deconnexion() {
		$this->logOut();
		$this->isLogged();
	}

	public function login(){
		$this->layout = "loginLayout";

		if($_POST) 
		{
			$this->loadModel('user');
			/* Vérification de l'utilisateur enregistré en BDD */
			$result['user'] = $this->user->verifAuth($_POST['email'],$_POST['password']);

			if($result['user']->id)
			{
				$_SESSION['auth'] = session_id();
				$_SESSION['iduser'] = $result['user']->id;
				$_SESSION['nom'] = $result['user']->nom;
				$_SESSION['identreprise'] = $result['user']->id_entreprise;
				redirigePHP(RACINE.'adminBt/accueil');

			}
		}

		if($_SESSION['authpublic']==session_id())
			redirigePHP(RACINE.'menu');
	}

	

	function profil() {
		$this->isLogged();
		$this->loadModel('profil');
		$this->loadModel('intitule');
		$this->loadModel('empathie');
		$this->loadModel('flexibilite');

		$result['intitules'] = $this->intitule->getIntitules();
		foreach($result['intitules'] as $key=>$val)
		{
			$intitules[$val->id]['id']=$val->id;
			$intitules[$val->id]['gauche_fr']=$val->gauche_fr;
			$intitules[$val->id]['droite_fr']=$val->droite_fr;
			$intitules[$val->id]['gauche_en']=$val->gauche_en;
			$intitules[$val->id]['droite_en']=$val->droite_en;
		}
		$result['empathies'] = $this->empathie->getEmpathies();
		foreach($result['empathies'] as $key=>$val)
		{
			$empathies[$val->id]['id']=$val->id;
			$empathies[$val->id]['gauche_fr']=$val->gauche_fr;
			$empathies[$val->id]['droite_fr']=$val->droite_fr;
			$empathies[$val->id]['gauche_en']=$val->gauche_en;
			$empathies[$val->id]['droite_en']=$val->droite_en;
		}

		$result['flexibilite'] = $this->flexibilite->getFlexibilites();
		foreach($result['flexibilites'] as $key=>$val)
		{
			$flexibilite[$val->id]['id']=$val->id;
			$flexibilite[$val->id]['gauche_fr']=$val->gauche_fr;
			$flexibilite[$val->id]['droite_fr']=$val->droite_fr;
			$flexibilite[$val->id]['gauche_en']=$val->gauche_en;
			$flexibilite[$val->id]['droite_en']=$val->droite_en;
		}


		$results['profil'] = $this->profil->getProfil($this->request->params['idprofil']);
		$results['profils_additionnels'] = $this->profil->getProfilsAdditionnels($results['profil']->code_secret);
		$this->setParamsView('intitules',$intitules);
		$this->setParamsView('empathies',$empathies);
		$this->setParamsView('flexibilite',$flexibilite);
		$this->setparamsView($results);

	}



}

?>
