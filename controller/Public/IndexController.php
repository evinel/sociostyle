<?php
error_reporting(0);

class IndexController extends Controller {



	function index() {
		//session_destroy();
		if($_POST['submit']){
			$_SESSION['nomprenom']="";
			$_SESSION['email']="";
			$_SESSION['profil_additionnel']="";
			if($_GET['code_secret'])
				$_SESSION['profil_additionnel'] =$_GET['code_secret'];
			$_SESSION['nomprenom'] =$_POST['nomprenom'];
			redirigePHP(RACINE.'repondre');
		}
	}


	function repondre(){
		/* Enregistrement des reponses */
		if($_POST['amount']){
			$this->loadModel('profil');
			$_POST['grille_reponses'] = serialize($_POST['amount']);
			$_POST['nomprenom']=$_SESSION['nomprenom'];
			$_POST['date_reponse']=date('Y-m-d H:i:s');
			if($_SESSION['profil_additionnel'])
				$_POST['profil_additionnel']=$_SESSION['profil_additionnel'];	
			else
				$_POST['code_secret']=hash('crc32',$_POST['nomprenom'].$_POST['grille_reponses'].date('YmdHis'));	

			for($i=1; $i<=10;$i++){	$MoyH += $_POST['amount'][$i]; } 
				$MoyH = ($MoyH/10)-1.5;

			for($i=11; $i<=20;$i++){	$MoyV += $_POST['amount'][$i]; } 
				$MoyV = ($MoyV/10)-1.5;

			for($i=21; $i<=30;$i++){	$MoyD1 += $_POST['amount'][$i]; } 
				$MoyD1 = ($MoyD1/10)-1.5;

			for($i=31; $i<=40;$i++){	$MoyD2 += $_POST['amount'][$i]; } 
				$MoyD2 = ($MoyD2/10)-1.5;

			$C1= ($MoyD1+$MoyD2)/sqrt(2);
			$C2= (-($MoyD1)+$MoyD2)/sqrt(2);

			$_POST['coord_x'] = ($MoyH+$C1)/2;
			$_POST['coord_y'] = ($MoyV+$C2)/2;

            $empathieSumMoy = array_sum($_POST['amount-empathie-calcule'])/count($_POST['amount-empathie-calcule']);
          //  $empathieSumMoy = array_sum($_POST['amount-empathie'])/count($_POST['amount-empathie']);
            $_POST['grille_reponse_empathie'] = serialize($_POST['amount-empathie']);
            $_POST['empathie_moy'] =round($empathieSumMoy,1);


            //flexibilité
            $flexibiliteSumMoy = array_sum($_POST['amount-flexibilite-calcule'])/count($_POST['amount-flexibilite-calcule']);
            $_POST['grille_reponse_flexibilite'] = serialize($_POST['amount-flexibilite']);
            $_POST['flexibilite_moy'] =round( $flexibiliteSumMoy,1);

            $this->profil->saveBdd($_POST,'profils');
			if($_SESSION['profil_additionnel'])
				redirigePHP('merci');
			else
				redirigePHP('partage/?code_secret='.$_POST['code_secret']);
		}
		/* fin d'enregistrement */

		$this->loadModel('intitule');
		$results['intitules'] = $this->intitule->getIntitules();
		if($_SESSION['profil_additionnel'])
		{
			$this->loadModel('profil');
			$result['profil_additionnel'] = $this->profil->getProfilByCodeSecret($_SESSION['profil_additionnel']);
		}
		foreach($results['intitules'] as $key=>$val)
		{
			$intitules[$val->id]['id']=$val->id;
			$intitules[$val->id]['gauche_fr']=$val->gauche_fr;
			$intitules[$val->id]['droite_fr']=$val->droite_fr;
			$intitules[$val->id]['gauche_en']=$val->gauche_en;
			$intitules[$val->id]['droite_en']=$val->droite_en;
		}
        $this->loadModel('empathie');
        $results['empathies'] = $this->empathie->getEmpathies();
        foreach($results['empathies'] as $key=>$val)
        {
            $empathies[$val->id]['id']=$val->id;
            $empathies[$val->id]['gauche_fr']=$val->gauche_fr;
            $empathies[$val->id]['droite_fr']=$val->droite_fr;
            $empathies[$val->id]['gauche_en']=$val->gauche_en;
            $empathies[$val->id]['droite_en']=$val->droite_en;
        }

        $this->loadModel('flexibilite');
        $results['flexibilites'] = $this->flexibilite->getFlexibilites();
        foreach($results['flexibilites'] as $key=>$val)
        {
            $flexibilites[$val->id]['id']=$val->id;
            $flexibilites[$val->id]['gauche_fr']=$val->gauche_fr;
            $flexibilites[$val->id]['droite_fr']=$val->droite_fr;
            $flexibilites[$val->id]['gauche_en']=$val->gauche_en;
            $flexibilites[$val->id]['droite_en']=$val->droite_en;
        }

        $this->setParamsView('intitules',$intitules);
        $this->setParamsView('empathies',$empathies);
        $this->setParamsView('flexibilites',$flexibilites);
        $this->setParamsView($result);
	}

	function merci(){

	}

	function partage(){

	}


}
?>