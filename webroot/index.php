<?php
error_reporting(0);

session_start();
$debut = microtime(true);

define('REDIR_ROOT', dirname(__FILE__));
define('ROOT', dirname(REDIR_ROOT));
define('DS', DIRECTORY_SEPARATOR);
define('CORE', ROOT . DS . 'core');

define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME'])));

define('RACINE','http://'.$_SERVER['SERVER_NAME'].'/'.trim(BASE_URL, DS)."/");

// PROD
//define('RACINESSO','http://sso.si2su.com/');

//DEV
define('RACINESSO','http://192.168.1.91/sso.si2su.com/main/');

//BOITIER
//define('RACINESSO','http://10.0.0.1/sso/');
//define('RACINESSO','http://appybox.lan/');

define('ID_APP',"6");

//define('WEBROOT','http://'.$_SERVER['SERVER_NAME'].BASE_URL.'/webroot/');
//define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME']))); /* /tuto_mvc chemin vers le script */





require CORE . DS . 'Includes.php';

SecureEnv();
new Dispatcher();

?>

<?php if(Conf::$tpsAffichage==1): ?>

	<div style="position:fixed; bottom:0; background:#900; color:#FFF; line-height:30px; height:30px; left:0; right:0; padding-left:10px;">
		<?php
		echo 'Page générée en ' . round(microtime(true) - $debut, 5) . ' secondes';
		?>
	</div>

<?php endif ?>
